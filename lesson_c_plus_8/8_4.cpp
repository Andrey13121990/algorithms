#include <iostream>


void insert_sort (int A[], int N) {
    
    for (int pos = 1; pos < N; pos ++) {
        int i = pos;
        while (i > 0 and A[i - 1] > A [i]) {
            int tmp = A[i];
            A[i] = A[i - 1];
            A[i - 1] = tmp;
            i -= 1;
        }
    }
}


void print_array(int A[], int N){
    using namespace std;

    for (int i = 0; i < N; i++){        
        cout << A[i] << '\t';
        }
    
    cout << '\n';
}


int main() {

    int N = 10;
    int A[] = {7, 5, 3, 6, 8, 1, 7, 2, 4, 10};

    insert_sort(A, N);
    print_array(A, N);

    return 0;
} 