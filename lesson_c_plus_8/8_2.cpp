#include <iostream>


int find_left_bound (int A[], int N, int x) {
    
    int left = -1;
    int right = N;

    while (right - left > 1) {
        int middle = (left + right) / 2;
        if (A[middle] < x)
            left = middle;

        else
            right = middle;
    }

    return left;
}


int find (int A[], int N, int x) {

    int left_bound = find_left_bound (A, N, x);
    int potential_first_index = left_bound + 1;

    if (potential_first_index < N and 
        A[potential_first_index] == x)
        return potential_first_index;
    else
        return -1;
    
}


int main() {

    using namespace std;
    int N = 10;
    int A[] = {1, 2, 3, 7, 7, 7, 7, 9, 10, 10};
    int x;


    cin >> x; 

    cout << "left bound of x is: " << find_left_bound (A, N, x) << '\n';
    cout << "x placement is: " << find (A, N, x) << '\n';

    return 0;
}    