#include <iostream>


void choice_sort (int A[], int N) {
    for (int pos = 0; pos < N - 1; pos ++){
        
        for (int i = pos +1; i < N; i++){
            
            if (A[i] < A[pos]) {
                int tmp = A[i];
                A[i] = A[pos];
                A[pos] = tmp;
            }
        }
    }
}


void print_array(int A[], int N){
    using namespace std;

    for (int i = 0; i < N; i++){        
        cout << A[i] << '\t';
        }
    
    cout << '\n';
}


int main() {

    int N = 10;
    int A[] = {7, 5, 3, 6, 8, 1, 7, 2, 4, 10};

    choice_sort(A, N);
    print_array(A, N);

    return 0;
} 