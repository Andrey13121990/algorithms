#include <iostream>


int find (int A[], int N, int x) {
    
    for (int i = 0; i < N; i ++) {
        if (A[i] == x) {
            return i;
        }
    }

    return -1;
}


int main() {

    using namespace std;
    int N = 10;
    int A[] = {1, 2, 3, 7, 7, 7, 7, 9, 10, 10};
    int x;


    cin >> x; 

    cout << "x index is: " << find (A, N, x) << '\n';

    return 0;
}    