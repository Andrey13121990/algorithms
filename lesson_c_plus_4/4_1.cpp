#include <iostream>


void increment (int *a){
    *a = *a + 1;
}


int main () {

    int b = 3;
    increment (&b);

    std::cout << b << std::endl;

    return 0;
}