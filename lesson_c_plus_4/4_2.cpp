#include <iostream>


struct t_Pair {
    int a;
    int b;
};


t_Pair return_pair (int x) {
    t_Pair result;

    result.a = x * x ;
    result.b = x * x * x;
    return result;
}


int main () {

    int x = 10; 
    t_Pair res = return_pair (x);
    std::cout << res.a << ' ' << res.b << std::endl;

    return 0;
}