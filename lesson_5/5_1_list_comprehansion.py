from time import time


A = [x for x in range(1000000)]


t0 = time()

B = []
for item in A:
    B.append(item**2)

print (time() - t0)


t0 = time()

C = [x**2 for x in A]

print (time() - t0)