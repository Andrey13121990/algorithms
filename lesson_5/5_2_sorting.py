

def insert_sort(data: list) -> list:
    """Сортировка вставками"""
    n = len(data)

    for item in range (1, n):
        k = item
        while k > 0 and data[k-1] > data[k]:
            data[k], data[k-1] = data[k-1], data[k]
            k -= 1
    return data


def choise_sort(data: list) -> list:
    """Сортировка выбором"""
    n = len(data)

    for item in range (0, n-1):
        for k in range(item+1, n):
            if data[k] < data[item]:
                data[k], data[item] = data[item], data[k]
    
    return data


def bubble_sort(data:list) -> list:
    """Сортировка пузырьком"""
    n = len(data)

    for item in range (1, n):
        for k in range(0, n-item):
            if data[k] > data[k+1]:
                data[k], data[k+1] = data[k+1], data[k]

    return data


def count_sort(data:list) -> list:
    F = []
    for i in range (10):
        x = int(input())
        F[x] += 1

        #TODO add realization


if __name__ == "__main__":
    
    def test_sort(algorithm):

        print(algorithm.__doc__)
        a = [1, 7, 23, -23, 4, 1.1, 74, -3, 7, 13, -4 ,8]
        a_sorted = [-23, -4, -3, 1, 1.1, 4, 7, 7, 8, 13, 23, 74]  

        if algorithm(a) == a_sorted:
            print ('Test --- OK\n')
        else:
            print('Test --- fail\n')

    test_sort(insert_sort)
    test_sort(choise_sort)
    test_sort(bubble_sort)