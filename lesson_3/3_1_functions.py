def hello(name = 'Guest', sep = ', '):
    '''
    return: user greeting
    name: enter name
    '''
    
    print ('Hello', name, sep=sep)


if __name__ == "__main__":
    hello(name = 'Andrey')
    hello()