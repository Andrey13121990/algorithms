

def is_simple_number(x: int) -> bool:
    '''
    Is this number simple or not:
     - If simple return True
     - Else return False
    '''

    devisor = 2

    if type(x) != int or x < 0:
        return False
    
    else:
        while devisor < x:
            if x%devisor == 0:
                return False
            devisor +=1
        return True


def print_is_simple_numbers(*args) -> None:

    for arg in args:
        a = is_simple_number(arg)
        print (f'Number {arg} is simple: {a} \n')   


def factorize_number(x: int) -> list:
    '''
    Devide number into simple factors
    Return list of factors
    '''

    if type(x) != int:
        return print('TYPE ERROR: enter integer number')

    elif x < 1:
        return print('TYPE ERROR: enter integer number > 0')

    else:
        devisor = 2
        factor_list = []
        while x > 1: 
            if x%devisor == 0:
                factor_list.append(devisor)
                x //= devisor
            else:
                devisor +=1
        
        return factor_list


if __name__ == "__main__":
    
    print_is_simple_numbers(1, 5, 6, 2.5, -3)
    print(factorize_number(0))
