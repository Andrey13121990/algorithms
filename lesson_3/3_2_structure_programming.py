

def build_house(
    window,
    left_corner,
    width):
    '''
    Function for house building
    '''

    height = calculate_height(width)

    return print('Home is done!')


def calculate_height(width: int) -> int:
    '''
    Function for height calculation 
    according to the width
    '''
    pass


if __name__ == "__main__":
    
    build_house(
        window = 1,
        left_corner = 2,
        width = 3)