#include <iostream>


int generator(int start, int end, int step)
{
    using namespace std;

    int x;

    x = start;
    while (x < end){
        cout << x << "\n";
        x += step; 
    }
    return 0;
}


int generator_2(int start, int end, int step)
{
    using namespace std;

    for (int x = start; 
        x < end; 
        x += step){
            cout << x << "\n";
        }
    return 0;
}


int main()
{
    generator(1, 20, 2);
    generator_2(1, 5, 1);
}