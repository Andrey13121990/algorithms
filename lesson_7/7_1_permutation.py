# Кол-во перестановок N чисел 


def generate_numbers(count: int,
                    lenght: int,
                    prefix: list = None):

    prefix = prefix or []

    if lenght == 0:
        print(prefix)
        return

    for digit in range(count):
        prefix.append(digit)
        generate_numbers(count, lenght - 1, prefix)
        prefix.pop()



def generate_permutations(count: int,
                        lenght: int = -1,
                        prefix: list = None):
    
    lenght == count if lenght == -1 else lenght 
    prefix = prefix or []

    if lenght == 0:
        print(prefix)
        return

    for number in range(1, count + 1):
        if number in prefix:
            continue
        prefix.append(number)
        generate_permutations(count, lenght-1, prefix)
        prefix.pop()



if __name__ == "__main__":
    generate_numbers(2, 3)
    generate_permutations(4, 4)