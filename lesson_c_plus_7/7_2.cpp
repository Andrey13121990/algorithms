#include <iostream>


const int M_max = 100;


void print_array(int A[][M_max], int N, int M){
    using namespace std;

    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++) {
            cout << A[i][j] << '\t';
        }
        cout << '\n';
    }
}


int main() {
    using namespace std;

    int N, M;
    cin >> N >> M;

    int A[N][M_max];
    
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            A[i][j] = i * M + j + 1;
        }
        
    }

    print_array(A, N, M);

    int *p = reinterpret_cast <int *> (A);

    for (int i = 0; i < M*N; i++){
        cout << * (p + i) << ' ';
    }

    cout << endl;

    return 0;
}