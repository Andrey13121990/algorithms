#include <iostream>


int main() {
    using namespace std;

    int N, M;
    cin >> N >> M;

    int **A = new int *[N];

    for (int i = 0; i < N; i++){
        A[i] = new int[M];
    }
    
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            A[i][j] = i * M + j + 1;
        }
        
    }

    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++) {
            cout << A[i][j] << '\t';
        }
        cout << '\n';
    }

    cout << endl;

    for (int i; i < N; i++){
        delete[] A[i];
    }

    delete[] A;

    return 0;
}