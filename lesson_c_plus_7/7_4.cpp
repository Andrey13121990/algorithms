#include <iostream>



void print_array(int *A, int N, int M){
    using namespace std;

    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++) {
            cout << A[i * M + j] << '\t';
        }
        cout << '\n';
    }
}


int main() {
    using namespace std;

    int N, M;
    cin >> N >> M;

    int A[N * M];
    
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            A[i * M + j] = i * M + j + 1;
        }
        
    }

    print_array(A, N, M);
    cout << endl;

    return 0;
}