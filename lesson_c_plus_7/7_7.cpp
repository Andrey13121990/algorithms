#include <iostream>


void print_array(int *A, int N, int M){
    using namespace std;

    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++) {
            cout << A[i * M + j] << '\t';
        }
        cout << '\n';
    }
}


int main() {
    using namespace std;

    int N, M;
    cin >> N >> M;

    int **A = new int *[N];
    A[0] = new int[N * M];

    for (int i = 0; i < N; i++){
        A[i] = A[0] + M * i;
    }
    
    for (int i = 0; i < N; i++){
        for (int j = 0; j < M; j++){
            A[i][j] = i * M + j + 1;
        }
        
    }

    print_array(A[0], M, N);

    cout << endl;

    delete[] A[0];
    delete[] A;

    return 0;
}