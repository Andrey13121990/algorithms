

def right_shift(A:list) -> list:
   
    N = len(A)
    tmp = A[0]

    for k in range(N-1):
        A[k] = A[k+1]

    A[N-1] = tmp
    return A


def test_right_shift() -> None:

    A = [1, 2, 3, 4, 5]
    A2 = right_shift(A)

    if A2 == [2, 3, 4, 5, 1]:
        print('Test 1 --- ok')
    else:
        print('Test 1 --- fail') 


if __name__ == "__main__":
    test_right_shift()