
def array_search(
    A: list, 
    N: int, 
    x: int) -> int:
    '''
    Find x in list A in range from 0 to N
    Return first x index in list A 
    If empty return -1
    '''

    for k in range (N):
        if A[k] == x:
            return k
    
    return -1


def test_array_search():
    A1 = [1, 2, 3, 4, 5]
    m = array_search(A = A1,
                    N = 5,
                    x = 8)
    if m == -1:
        print ('test 1 --- ok')
    else:
        print ('test 1 --- fail')


    A2 = [-1, -2, -3, -4, -5]
    m = array_search(A = A2,
                    N = 5,
                    x = -3)
    if m == 2:
        print ('test 2 --- ok')
    else:
        print ('test 2 --- fail')


    A3 = [10, 20, 30, 10, 10]
    m = array_search(A = A3,
                    N = 5,
                    x = 10)
    if m == 0:
        print ('test 2 --- ok')
    else:
        print ('test 2 --- fail')


if __name__ == "__main__":
    test_array_search()