

def invert_array(A:list, N:int) -> list:

    for k in range (N//2):
        A[k], A[N-k-1] = A[N-k-1], A[k]
        
    return A


def test_invert_array() -> None:

    A1 = [1, 2, 3, 4, 5]
    A2 = invert_array(A1, 5)

    if A2 == [5, 4, 3 ,2 , 1]:
        print('Test 1 --- ok')
    else:
        print('Test 1 --- fail') 


if __name__ == "__main__":
    test_invert_array()