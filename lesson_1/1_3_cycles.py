
# Цикл While

x = 10

while x < 15:  
    
    if x == 13:
        break
    
    else:
        x += 1
    
    print(x)



# Цикл For

for item in range(1, 15, 3):
    print(item**item)