# Наибольший общий делитель 


def gcd(a, b):
    if a == b:
        return a 

    elif a > b:
        return gcd(a-b, b)
    
    else:
        return gcd(a, b-a)


def gcd_2(a, b):
    return (a if b==0 else gcd(b, a%b))


if __name__ == "__main__":
    print(gcd(10, 4))
    print(gcd_2(10, 4))