

def matreshka(n):
    if n == 0:
        print('The most little one')
    else:
        print('1-st part of example n = %s' % n)
        matreshka(n-1)


if __name__ == "__main__":
    matreshka(10)