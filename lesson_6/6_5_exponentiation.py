# Быстрое возведение в степень


def quick_exp(a, n):


    if n == 0:
        return 1  

    elif n%2 == 1:
        return quick_exp(a, n-1)*a

    else:
        return quick_exp(a**2, n//2)


if __name__ == "__main__":
    print(quick_exp(2,100))