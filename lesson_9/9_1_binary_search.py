
# for sorted list


def left_bound(data: list, key):
    left = -1
    right = len(data)

    while right - left > 1:
        middle = (left + right) // 2
        if data[middle] < key:
            left = middle
        
        else:
            right = middle

    return left


def right_bound(data: list, key):
    left = -1
    right = len(data)

    while right - left > 1:
        middle = (left + right) // 2
        if data[middle] <= key:
            left = middle
        
        else:
            right = middle

    return right


def binary_search(data: list, key):
    left = left_bound(data, key)
    right = right_bound(data, key)

    if right - left > 1:
        is_exist = True
    else:
        is_exist = False

    return (is_exist,left, right)



if __name__ == "__main__":
    list_1 = [2, 2, 2, 3, 4, 4, 4, 6, 8, 8, 8]

    print(binary_search(list_1, 2))
    print(binary_search(list_1, 3))
    print(binary_search(list_1, 5))
    print(binary_search(list_1, 8))
