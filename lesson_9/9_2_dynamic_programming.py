# рассчитываем число фибоначчи


def fib(n): # сложность O(fib)
    if n <= 1:
        return n
    else:
        return fib(n-1) + fib(n-2)


def fib_2(n):

    data = [0,1]

    for item in range(2,n+1):
        next_num = data[-1] + data[-2]
        data.append(next_num)
    
    return data[n]


if __name__ == "__main__":
    print(fib(10))
    print(fib_2(10))