

def is_sorted(data: list, ascending = True):
    
    flag = True

    sign = 2*int(ascending) - 1
    
    for item in range(0, len(data)-1):
        if sign * data[item] > sign * data[item+1]:
            flag = False
            break

    return flag



if __name__ == "__main__":
    list_1 = [2, 3, 4, 6, 8]
    list_2 = [4, 5, 7, 6, 10]

    print(is_sorted(list_1))
    print(is_sorted(list_2))