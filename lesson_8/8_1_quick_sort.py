

def tony_hoar_sort(data: list):
    if len(data) <= 1:
        return
    
    else:
        barrier = data[0]

        left = list()
        middle = list()
        right = list()
        
        for item in data:
            if item < barrier:
                left.append(item)

            elif item == barrier:
                middle.append(item)

            else:
                right.append(item)

        tony_hoar_sort(left)
        tony_hoar_sort(right)

        i = 0
        for item in left + middle + right:
            data[i] = item
            i += 1
        
    return data



if __name__ == "__main__":

    list_1 = [2, 5, 1, 8, 5, 6, 3, 8]
    print(tony_hoar_sort(list_1))
