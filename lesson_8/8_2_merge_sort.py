

def merge(list_1: list, list_2: list):
    
    result = [0] * (len(list_1) + len(list_2))

    i1 = i2 = i = 0

    while i1 < len(list_1) and i2 < len(list_2):
        
        if list_1[i1] <= list_2[i2]:
            result[i] = list_1[i1]
            i1 += 1
            i += 1
        
        else:
            result[i] = list_2[i2]
            i2 += 1
            i += 1

    while i1 < len(list_1):
        result[i] = list_1[i1]
        i1 += 1
        i += 1

    while i2 < len(list_2):
        result[i] = list_2[i2]
        i2 += 1
        i += 1
    
    return result



def merge_sort(data: list):
    
    if len(data) <= 1:
        return
    
    else:
        middle = len(data) // 2
        left = [data[i] for i in range(0, middle)]
        right = [data[i] for i in range(middle, len(data))]

        merge_sort(left)
        merge_sort(right)

        results = merge(left, right)

        for item in range(len(data)):
            data[item] = results[item]
    
    return data



if __name__ == "__main__":
    list_1 = [2, 3, 4, 6, 8]
    list_2 = [4, 5, 7, 8, 10]

    print(merge(list_1, list_2))


    list_3 = [2, 5, 1, 8, 5, 6, 3, 8]
    print(merge_sort(list_3))