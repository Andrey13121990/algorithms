#include <iostream>


int main(){

    using namespace std;
    int N = 5;
    
    int A[5] = {1, 2, 3, 4, 5};

    for (int i = 0; i < N/2; ++i) {
        int tmp = A[i];
        A[i] = A[N-1-i];
        A[N-1-i] = tmp;
    }

    for (int i=0; i < N; ++i) {
        cout << A[i] << '\t';
    }

    cout << '\n';

    return 0;
}