#include <iostream>


int main(){

    using namespace std;

    int N = 5;    
    int A[5] = {5, 3, 2, 4, 1};

    bool is_sorted = false;

    while (not is_sorted){

        int i = 0;
        is_sorted = true;

        while (i < N-1) {
            
            if (A[i] > A[i+1]){
                int tmp = A[i];
                A[i] = A[i+1];
                A[i+1] = tmp;
                is_sorted = false;
            }
            i++;
        }
    }

    for (int i=0; i < N; ++i) {
        cout << A[i] << '\t';
        }

    cout << '\n';

    return 0;
}