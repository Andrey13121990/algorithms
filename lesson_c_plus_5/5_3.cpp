#include <iostream>


const int MAX_A_SIZE = 100;

int main(){

    using namespace std;
    int N = MAX_A_SIZE;
    
    int A[N];
    int x;
    int top = 0;
    cin >> x;

    while (x != 0) {
        A[top] = x;
        top += 1;
        cin >> x;
    }


    while (top > 0) {
        cout << A[--top] << '\t';
    }

    cout << '\n';

    return 0;
}